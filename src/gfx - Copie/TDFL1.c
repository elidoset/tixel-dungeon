// Converted using ConvPNG
#include <stdint.h>
#include "TDFL1.h"

#include <fileioc.h>
uint8_t *TDFL1[2] = {
 (uint8_t*)0,
 (uint8_t*)2838,
};

bool TDFL1_init(void) {
    unsigned int data, i;
    ti_var_t appvar;

    ti_CloseAll();

    appvar = ti_Open("TDFL1", "r");
    data = (unsigned int)ti_GetDataPtr(appvar) - (unsigned int)TDFL1[0];
    for (i = 0; i < TDFL1_num; i++) {
        TDFL1[i] += data;
    }

    ti_CloseAll();

    return (bool)appvar;
}

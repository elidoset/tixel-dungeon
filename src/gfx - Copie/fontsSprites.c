// Converted using ConvPNG
#include <stdint.h>
#include "fontsSprites.h"

uint16_t fontsSprites_pal[3] = {
 0x7C00,  // 00 :: rgb(255,0,0)
 0xFFFF,  // 01 :: rgb(255,255,255)
 0xB58D,  // 02 :: rgb(103,103,103)
};
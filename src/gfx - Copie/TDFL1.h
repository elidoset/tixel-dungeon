// Converted using ConvPNG
// This file contains all the graphics sources for easier inclusion in a project
#ifndef __TDFL1__
#define __TDFL1__
#include <stdint.h>

#include <stdbool.h>

#define TDFL1_num 2

extern uint8_t *TDFL1[2];
#define tiles1 ((gfx_sprite_t*)TDFL1[0])
#define sizeof_fl1_pal 440
#define fl1_pal ((uint16_t*)TDFL1[2])
bool TDFL1_init(void);

#endif

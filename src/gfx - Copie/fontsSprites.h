// Converted using ConvPNG
// This file contains all the graphics sources for easier inclusion in a project
#ifndef __fontsSprites__
#define __fontsSprites__
#include <stdint.h>

#define fontsSprites_transparent_color_index 0

#define font_width 146
#define font_height 20
#define font_size 2922
extern uint8_t font_data[2922];
#define font ((gfx_sprite_t*)font_data)
#define sizeof_fontsSprites_pal 6
extern uint16_t fontsSprites_pal[3];

#endif
